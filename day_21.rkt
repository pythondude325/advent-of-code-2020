k#lang racket/base

(require "aoc.rkt"
         racket/match
         racket/string
         racket/set
         racket/dict
         racket/symbol)

(struct food (ingredients allergens) #:transparent)

(define-match-expander ~>split
  (syntax-rules ()
    [(_ sep pat)
     (? string? (app (λ (s) (string-split s sep))
                     pat))]))

(define/match (parse-ingredient-list str)
  [((regexp #rx"^((?:[a-z]+ ?)+) \\(contains ((?:[a-z]+(?:, )?)+)\\)$"
            (list _
                  (~>split " " (list (~>symbol ingredients) ...))
                  (~>split ", " (list (~>symbol allergens) ...)))))
   (food (list->set ingredients) (list->set allergens))])

(define (day-21)
  (define foods (read-input-file "day_21_input.txt" (map-line parse-ingredient-list)))
  (define ingredients (foldl (λ (food ingredients) (set-union ingredients (food-ingredients food))) (set) foods))
  (define allergens (foldl (λ (food allergens) (set-union allergens (food-allergens food))) (set) foods))

  (define allergen-possible-causes (make-immutable-hash (for/list ([allergen allergens])
                                                          (cons allergen
                                                                (foldl (λ (food possible-causes) (set-intersect possible-causes (food-ingredients food)))
                                                                       ingredients
                                                                       (filter (λ (f) (set-member? (food-allergens f) allergen))
                                                                               foods)))
                                                          )))

  (define clean-ingredients (for/fold ([clean-ingredients ingredients])
                                      ([(allergen possible-causes) allergen-possible-causes])
                              (set-subtract clean-ingredients possible-causes)))
  
  (define part-1 (for/sum ([food foods])
                   (set-count (set-intersect clean-ingredients (food-ingredients food)))))

  (define allergen-causes (do ([allergen-causes (hash)]
                               [allergen-possible-causes allergen-possible-causes])
                            ((dict-empty? allergen-possible-causes) allergen-causes)
                            (match-let* ([(cons allergen (app set->list (list cause))) (for/first ([(allergen possible-causes) allergen-possible-causes]
                                                                                                   #:when (= 1 (set-count possible-causes)))
                                                                                         (cons allergen possible-causes))])
                              (set! allergen-causes (dict-set allergen-causes allergen cause))
                              (set! allergen-possible-causes (dict-remove allergen-possible-causes allergen))
                              (set! allergen-possible-causes (for/hash ([(allergen possible-causes) allergen-possible-causes])
                                                               (values allergen (set-remove possible-causes cause))))
                              )))

  (define part-2 (string-join (map (compose symbol->immutable-string cdr)
                                   (sort (hash->list allergen-causes) symbol<? #:key car))
                              ","))

  (show-vars part-1 part-2))

(day-21)